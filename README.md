# Installation

## Linux

This packages is not supported by the standard channels and tools provided by
TeX Live, such as CTAN repos and tlmgr. To ease the installation process a
Python 3 script is provided which installs the packages in the home root of
the TeX Live installation (usually located at ~/texmf).

This avoids to run the database update tool of TeX Live each time you update
these or any other packages inside the TEXMFHOME directory. So once installed
they're ready to use.

To run the script, simply do

	python3 install.py

then you will be asked to locate the TEXMFHOME directory, unless the
env variable TEXMFHOME is set.

## Mac OS X & Windows

This script **DOES NOT** work on Windows/OS X yet. Please install the packages
_as they are structured_ in your personal texmf directory. (e.g. everything
inside the 'doc' folder must go inside 'texmf/doc')

# Help & Support

As stated in the license ('LICENSE.txt') given with the archive, this
collection is not guaranteed to work (more details in the LICENSE.txt file).

If you however find any bug feel free to report them [here][1].

If you want to know how to use any particular package, use your usual command
from the terminal, i.e.

	texdoc <packagename>

[1]: https://www.github.com/Vieler/physcollection/issues "Phycollection repo"
