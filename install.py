# 'install.py'
#
# Copytight (C) 2012 Marcello Massaro <massaro.marcello[AT]gmail.com>
#
# This is used to install in the home TeX directory, usually set as '~/texmf',
# the packages that comes with this script. Remember that if you install
# packages in the home directory (not the root or the local directory) of TeX
# there is no need to update the TeX Live database in order to use them.
#
# Besides, you won't mess with the main TeX distribution. That's a good thing
# since these particular packages are not maintained through the official
# channels and tools (CTAN and tlmgr).
#
# This script is licensed under the GNU GPLv3 or any later versions, see the
#
# --> http://www.gnu.org/licenses/gpl.txt
#
# for more information. A copy of the license is included with this archive.

import sys, os, shutil
from os.path import join as j
# Paths declaration
pkgs_dir = os.path.abspath(os.getcwd())
packages_docdir = j(pkgs_dir, 'doc')
packages_texdir = j(pkgs_dir, 'tex')
packages_latexdir = j(packages_texdir, 'latex/')
# Verification of the TEXMFHOME environment variable
TEXMFHOME = os.environ.get('TEXMFHOME')
# User selection
if TEXMFHOME is None:
    print("TEXMFHOME is not set.")
    go_on_dir = input("Would you like to manually insert local root of TeX?\
 (<n> to exit, <d> for default '~/texmf') ")
    if go_on_dir == 'n' or go_on_dir == 'N':
        exit(1)
    elif go_on_dir == 'd' or go_on_dir == 'D':
        TEXMFHOME = os.path.expanduser('~/texmf')
    else:
        TEXMFHOME = os.path.expanduser(go_on_dir)

os.chdir(TEXMFHOME)
# Check for existance of previous custom trees
if (not os.path.isdir('doc/')) and\
   (not os.path.isdir('tex/')):
    shutil.copytree(packages_docdir, j(TEXMFHOME, 'doc'))
    shutil.copytree(packages_latexdir, j(TEXMFHOME, 'tex/latex'))
else:
    package_docdir_list = os.listdir(packages_docdir)
    package_latex_list = os.listdir(packages_latexdir)
    for pk in package_docdir_list:
        # If the package is new, the entire tree is copied
        if (not os.path.isdir(j(TEXMFHOME, 'doc', pk))):
            try:
                shutil.copytree(j(packages_docdir, pk), j(TEXMFHOME, 'doc', pk))
            except OSError:
                print("Errore nell'installazione dei file: non si hanno i \
permessi per scrivere in %s" % TEXMFHOME)
                exit(1)
        # otherwise, each file is overwritten
        else:
            root_package_dir = j(packages_docdir, pk)
            package_file_list = os.listdir(root_package_dir)
            new_package_dir = j(TEXMFHOME, 'doc', pk)
            for file in package_file_list:
                try:
                    shutil.copy2(j(root_package_dir, file), new_package_dir)
                except OSError:
                    print("Errore nell'installazione dei file: non si hanno i permessi per scrivere in %s" % TEXMFHOME)
                    exit(1)
    # The same thing happens for the tex/latex subfolder
    for pk in package_latex_list:
        if (not os.path.isdir(j(TEXMFHOME, 'tex/latex', pk))):
            try:
                shutil.copytree(j(packages_docdir, pk), j(TEXMFHOME, 'tex/latex', pk))
            except OSError:
                print("Errore nell'installazione dei file: non si hanno i permessi per scrivere in %s" % TEXMFHOME)
                exit(1)
        else:
            root_package_dir = j(packages_latexdir, pk)
            package_file_list = os.listdir(root_package_dir)
            new_package_dir = j(TEXMFHOME, 'tex/latex', pk)
            for file in package_file_list:
                try:
                    shutil.copy2(j(root_package_dir, file), new_package_dir)
                except OSError:
                    print("Errore nell'installazione dei file: non si hanno i permessi per scrivere in %s" % TEXMFHOME)
                    exit(1)

print('Operation completed.')
